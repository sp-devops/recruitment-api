package com.softwareplant.recruitment.infrastructure;

import com.softwareplant.recruitment.domain.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource
public interface ProjectRepository extends JpaRepository<Project, Long> {

	@Override
	@RestResource(exported = false)
	void deleteById(Long id);

	@Override
	@RestResource(exported = false)
	Project save(Project project);
}
