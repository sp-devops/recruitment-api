package com.softwareplant.recruitment.infrastructure;

import com.softwareplant.recruitment.domain.User;
import com.softwareplant.recruitment.domain.Iteration;
import com.softwareplant.recruitment.domain.Portfolio;
import com.softwareplant.recruitment.domain.Project;
import com.softwareplant.recruitment.domain.Task;
import java.time.LocalDateTime;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SampleDataIntializer {

	private final PortfolioRepository portfolioRepository;
	private final UserRepository userRepository;

	public SampleDataIntializer(PortfolioRepository portfolioRepository, UserRepository userRepository) {
		this.portfolioRepository = portfolioRepository;
		this.userRepository = userRepository;
	}


	@EventListener
	public void appReady(ApplicationReadyEvent event) {
		var reporterUser = userRepository.save(new User("jack.mcgrath@softwareplant.com", "Jack", "McGrath"));
		var pmUser = userRepository.save(new User("alexander.allan@softwareplant.com", "Alexander", "Allan"));

		var portfolio = new Portfolio(
				"SoftwarePlant portfolio",
				"Portfolio of SoftwarePlant products.",
				pmUser
		);

		var bigPictureEnterprise = new Project(
				"BigPicture Enterprise",
				"Development and maintenance of BigPicture Enterprise product",
				LocalDateTime.MIN,
				LocalDateTime.MAX
		);
		portfolio.addProject(bigPictureEnterprise);

		var bigPicture = new Project(
				"BigPicture",
				"Development and maintenance of BigPicture product",
				LocalDateTime.MIN,
				LocalDateTime.MAX
		);
		portfolio.addProject(bigPicture);

		var bigTemplate = new Project(
				"BigTemplate",
				"Development and maintenance of BigTemplate product",
				LocalDateTime.MIN,
				LocalDateTime.MAX
		);
		portfolio.addProject(bigTemplate);

		var bigGantt = new Project(
				"BigGantt",
				"Development and maintenance of BigTemplate product",
				LocalDateTime.MIN,
				LocalDateTime.MAX
		);
		portfolio.addProject(bigGantt);

		bigPictureEnterprise.addTask(Task.createImprovement("milestone - Document technical and business requirements", "", 1, reporterUser));
		bigPictureEnterprise.addTask(Task.createImprovement("Verify that project deliverables meet the requirements", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createImprovement("Gather requirements from business units or users", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createImprovement("Assist in defining the project", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createImprovement("Analysis", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createImprovement("Lead and manage the project team", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createImprovement("Establish a project schedule", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createFeature("Develop a project plan", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createFeature("Project Management", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createFeature("Project plan", "some description", 2, reporterUser));
		bigPictureEnterprise.addTask(Task.createFeature("Planning and controlling of change, resources and deadlines", "some description", 2, reporterUser));

		bigPicture.addTask(Task.createFeature("Operations", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createFeature("Identify and interact with stakeholders", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createFeature("Identify and interact with stakeholders", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createFeature("Picking & executing the right modelling", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createFeature("Architecture", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createFeature("Measuring and reporting test coverage", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createFeature("Configuring, using, and managing test environments and test data", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createImprovement("Testing", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createImprovement("Testing", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createImprovement("[Story] Enhanced track information (elevation, max speed, pace etc.)", "some description", 2, reporterUser));
		bigPicture.addTask(Task.createImprovement("[Feature] Enhanced reporting", "some description", 2, reporterUser));

		var iteration1 = new Iteration("first iteration", LocalDateTime.now(), LocalDateTime.MAX);
		iteration1.addTask(Task.createFeature("[Story] Step count for walking", "some description", 2, reporterUser));
		iteration1.addTask(Task.createFeature("[Feature] Support for cycling, walking and interval training", "some description", 2, reporterUser));
		iteration1.addTask(Task.createFeature("[Story] Get user confirmation on the activity type", "some description", 2, reporterUser));
		iteration1.addTask(Task.createFeature("[Story] Step count for running", "some description", 2, reporterUser));
		iteration1.addTask(Task.createFeature("[Feature] Self-learning algorithm for running", "some description", 2, reporterUser));

		var iteration2 = new Iteration("second iteration", LocalDateTime.now(), LocalDateTime.MAX);
		iteration2.addTask(Task.createImprovement("[Story] Sound cues for activity type switch", "some description", 2, reporterUser));
		iteration2.addTask(Task.createImprovement("[Story] Notify user about identified activity type", "some description", 2, reporterUser));
		iteration2.addTask(Task.createImprovement("[Feature] Self-learning algorithm for identifying activity type", "some description", 2, reporterUser));
		iteration2.addTask(Task.createImprovement("[Story] Training log pages", "some description", 2, reporterUser));
		iteration2.addTask(Task.createImprovement("[Feature] Training log", "some description", 2, reporterUser));
		iteration2.addTask(Task.createImprovement("[Story] Track visualization on a map", "some description", 2, reporterUser));

		bigTemplate.addIteration(iteration1);
		bigTemplate.addIteration(iteration2);


		var iteration3 = new Iteration("first iteration", LocalDateTime.now(), LocalDateTime.MAX);
		var iteration4 = new Iteration("second iteration", LocalDateTime.now(), LocalDateTime.MAX);
		var iteration5 = new Iteration("third iteration", LocalDateTime.now(), LocalDateTime.MAX);

		bigGantt.addIteration(iteration3);
		bigGantt.addIteration(iteration4);
		bigGantt.addIteration(iteration5);

		iteration3.addTask(Task.createFeature("[Feature] Visualization of tracks on a map", "some description", 2, reporterUser));
		iteration3.addTask(Task.createFeature("[Story] Simple UI (start, stop, pause)", "some description", 2, reporterUser));
		iteration3.addTask(Task.createFeature("[Story] Storing GPS data locally", "some description", 2, reporterUser));
		iteration3.addTask(Task.createFeature("[Story] Tracking GPS data", "some description", 2, reporterUser));
		iteration3.addTask(Task.createFeature("[Feature] Tracking and registering GPS data", "some description", 2, reporterUser));
		iteration3.addTask(Task.createFeature("Architecture and development", "some description", 2, reporterUser));

		iteration4.addTask(Task.createImprovement("milestone - Document technical and business requirements", "some description", 2, reporterUser));
		iteration4.addTask(Task.createImprovement("Verify that project deliverables meet the requirements", "some description", 2, reporterUser));
		iteration4.addTask(Task.createImprovement("Gather requirements from business units or users", "some description", 2, reporterUser));
		iteration4.addTask(Task.createImprovement("Assist in defining the project", "some description", 2, reporterUser));
		iteration4.addTask(Task.createFeature("Lead and manage the project team", "some description", 2, reporterUser));
		iteration4.addTask(Task.createFeature("Establish a project schedule", "some description", 2, reporterUser));

		iteration5.addTask(Task.createImprovement("Develop a project plan", "some description", 2, reporterUser));
		iteration5.addTask(Task.createImprovement("Project Management", "some description", 2, reporterUser));
		iteration5.addTask(Task.createImprovement("Project plan", "some description", 2, reporterUser));
		iteration5.addTask(Task.createImprovement("Planning and controlling of change, resources and deadlines", "some description", 2, reporterUser));
		iteration5.addTask(Task.createImprovement("Planning and controlling of change, resources and deadlines", "some description", 2, reporterUser));

		portfolioRepository.save(portfolio);
	}
}