package com.softwareplant.recruitment.infrastructure;

import com.softwareplant.recruitment.domain.Portfolio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource
public interface PortfolioRepository extends JpaRepository<Portfolio, Long> {

	@Override
	@RestResource(exported = false)
	void deleteById(Long id);

	@Override
	@RestResource(exported = false)
	Portfolio save(Portfolio portfolio);
}
