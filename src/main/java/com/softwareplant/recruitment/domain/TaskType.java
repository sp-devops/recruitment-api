package com.softwareplant.recruitment.domain;

public enum TaskType {
	BUG, NEW_FEATURE, IMPROVEMENT
}
