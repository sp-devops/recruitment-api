package com.softwareplant.recruitment.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Getter;

@Entity
@Getter
public class Portfolio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String description;

	@ManyToOne
	private User owner;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Project> projects;

	public Portfolio() {
	}

	public Portfolio(String name, String description, User owner) {
		this.name = name;
		this.description = description;
		this.owner = owner;
		this.projects = new ArrayList<>();
	}

	public void addProject(Project project) {
		projects.add(project);
	}
}
