package com.softwareplant.recruitment.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;

@Entity
@Getter
public class Project {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private LocalDateTime createdAt;

	private LocalDateTime startDate;

	private LocalDateTime endDate;

	private String title;

	private String description;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Iteration> iterations;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Task> tasks;

	public Project() {
	}

	public Project(String title, String description, LocalDateTime startDate, LocalDateTime endDate){
		this.title = title;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.createdAt = LocalDateTime.now();
		this.iterations = new ArrayList<>();
		this.tasks = new ArrayList<>();
	}

	public void addIteration(Iteration iteration){
		iterations.add(iteration);
	}

	public void addTask(Task task){
		tasks.add(task);
	}
}
