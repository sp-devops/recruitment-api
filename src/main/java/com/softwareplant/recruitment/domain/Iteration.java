package com.softwareplant.recruitment.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;

@Entity
@Getter
public class Iteration {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;

	private LocalDateTime startDate;

	private LocalDateTime endDate;

	@OneToMany(cascade = CascadeType.ALL)
	private List<Task> tasks;

	public Iteration() {
	}

	public Iteration(String title, LocalDateTime startDate, LocalDateTime endDate){
		this.title = title;
		this.startDate = startDate;
		this.endDate = endDate;
		this.tasks = new ArrayList<>();
	}

	public void addTask(Task task){
		tasks.add(task);
	}
}
