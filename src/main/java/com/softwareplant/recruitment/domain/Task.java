package com.softwareplant.recruitment.domain;

import java.time.Period;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Getter;

@Entity
@Getter
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String title;

	private String description;

	private TaskType type;

	@ManyToOne
	private User assignee;

	@ManyToOne
	private User reporter;

	private Period estimationTime;

	private Period remainingTime;

	public Task() {
	}

	public Task(String title, String description, int estimationTimeInDays, User reporter, TaskType taskType){
		this.title = title;
		this.description = description;
		this.estimationTime = Period.ofDays(estimationTimeInDays);
		this.remainingTime = Period.ofDays(estimationTimeInDays);
		this.reporter = reporter;
		this.type = taskType;
	}

	public static Task createFeature(String title, String description, int estimationTimeInDays, User reporter){
		return new Task(title, description, estimationTimeInDays, reporter, TaskType.NEW_FEATURE);
	}

	public static Task createImprovement(String title, String description, int estimationTimeInDays, User reporter){
		return new Task(title, description, estimationTimeInDays, reporter, TaskType.IMPROVEMENT);
	}

	public void assign(User user){
		this.assignee = user;
	}
}
