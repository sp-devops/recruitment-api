package com.softwareplant.recruitment.app;

import com.softwareplant.recruitment.domain.Iteration;
import com.softwareplant.recruitment.domain.Portfolio;
import com.softwareplant.recruitment.domain.Project;
import com.softwareplant.recruitment.domain.Task;
import com.softwareplant.recruitment.domain.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class ResourceDataExpositionConfiguration {

	@Bean
	public RepositoryRestConfigurer repositoryRestConfigurer() {
		return RepositoryRestConfigurer.withConfig(config -> config.exposeIdsFor(
				Iteration.class,
				Portfolio.class,
				Project.class,
				Task.class,
				User.class
		));
	}
}
