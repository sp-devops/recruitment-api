FROM adoptopenjdk/openjdk11:alpine-jre
ADD target/recruitment-api-1.jar .
EXPOSE 8080
CMD java -jar recruitment-api-1.jar